<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231219104518 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE automation_rules_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE devices_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE energy_price_info_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE energy_usage_logs_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE rooms_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE scheduled_tasks_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE users_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE weather_data_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE automation_rules (id INT NOT NULL, user_id INT NOT NULL, rule_name VARCHAR(255) NOT NULL, rule_conditions JSON NOT NULL, is_active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX automation_rules__user_id__ind ON automation_rules (user_id)');
        $this->addSql('CREATE TABLE devices (id INT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, device_type VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, last_active TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX devices__user_id__ind ON devices (user_id)');
        $this->addSql('CREATE TABLE energy_price_info (id INT NOT NULL, time_period_start TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, time_period_end TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, price_per_unit DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE energy_usage_logs (id INT NOT NULL, device_id INT NOT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, energy_consumed DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX energy_usage_logs__device_id__ind ON energy_usage_logs (device_id)');
        $this->addSql('CREATE TABLE rooms (id INT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX rooms__user_id__ind ON rooms (user_id)');
        $this->addSql('CREATE TABLE scheduled_tasks (id INT NOT NULL, rule_id INT NOT NULL, scheduled_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, task_status VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX scheduled_tasks__rule_id__ind ON scheduled_tasks (rule_id)');
        $this->addSql('CREATE TABLE users (id INT NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9E7927C74 ON users (email)');
        $this->addSql('CREATE TABLE weather_data (id INT NOT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, temperature DOUBLE PRECISION NOT NULL, humidity DOUBLE PRECISION NOT NULL, weather_condition VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE messenger_messages (id BIGSERIAL NOT NULL, body TEXT NOT NULL, headers TEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql('CREATE OR REPLACE FUNCTION notify_messenger_messages() RETURNS TRIGGER AS $$
            BEGIN
                PERFORM pg_notify(\'messenger_messages\', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$ LANGUAGE plpgsql;');
        $this->addSql('DROP TRIGGER IF EXISTS notify_trigger ON messenger_messages;');
        $this->addSql('CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON messenger_messages FOR EACH ROW EXECUTE PROCEDURE notify_messenger_messages();');
        $this->addSql('ALTER TABLE automation_rules ADD CONSTRAINT automation_rules_fk_user_id FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE devices ADD CONSTRAINT devices_fk_user_id FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE energy_usage_logs ADD CONSTRAINT energy_usage_logs_fk_device_id FOREIGN KEY (device_id) REFERENCES devices (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rooms ADD CONSTRAINT rooms_fk_user_id FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE scheduled_tasks ADD CONSTRAINT scheduled_tasks_fk_rule_id FOREIGN KEY (rule_id) REFERENCES automation_rules (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE automation_rules_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE devices_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE energy_price_info_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE energy_usage_logs_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE rooms_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE scheduled_tasks_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE users_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE weather_data_id_seq CASCADE');
        $this->addSql('ALTER TABLE automation_rules DROP CONSTRAINT automation_rules_fk_user_id');
        $this->addSql('ALTER TABLE devices DROP CONSTRAINT devices_fk_user_id');
        $this->addSql('ALTER TABLE energy_usage_logs DROP CONSTRAINT energy_usage_logs_fk_device_id');
        $this->addSql('ALTER TABLE rooms DROP CONSTRAINT rooms_fk_user_id');
        $this->addSql('ALTER TABLE scheduled_tasks DROP CONSTRAINT scheduled_tasks_fk_rule_id');
        $this->addSql('DROP TABLE automation_rules');
        $this->addSql('DROP TABLE devices');
        $this->addSql('DROP TABLE energy_price_info');
        $this->addSql('DROP TABLE energy_usage_logs');
        $this->addSql('DROP TABLE rooms');
        $this->addSql('DROP TABLE scheduled_tasks');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE weather_data');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
