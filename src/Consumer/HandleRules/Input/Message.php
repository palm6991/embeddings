<?php

namespace App\Consumer\HandleRules\Input;

use Symfony\Component\Validator\Constraints as Assert;

class Message
{
    #[Assert\Type('string')]
    private string $eventType;

    #[Assert\Type('int')]
    private int $ruleId;

    #[Assert\Type('int')]
    private int $userId;

    public static function createFromQueue(string $messageBody): self
    {
        $message = json_decode($messageBody, true, 512, JSON_THROW_ON_ERROR);
        $result = new self();
        $result->eventType = $message['eventType'];
        $result->ruleId = $message['ruleId'];
        $result->userId = $message['userId'];

        return $result;
    }

    public function getEventType(): string
    {
        return $this->eventType;
    }

    public function getRuleId(): int
    {
        return $this->ruleId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
}