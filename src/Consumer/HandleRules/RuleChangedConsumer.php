<?php

namespace App\Consumer\HandleRules;

use App\Consumer\HandleRules\Input\Message;
use Exception;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RuleChangedConsumer implements ConsumerInterface
{

    /**
     * @throws Exception
     */
    public function __construct(
        private readonly CacheItemPoolInterface $cacheItemPool,
        private readonly ValidatorInterface $validator,
    )
    {

    }

    public function execute(AMQPMessage $msg): bool|int
    {
        echo "Received message: ", $msg->body, "\n";

        try {
            $message = Message::createFromQueue($msg->body);
            $errors = $this->validator->validate($message);

            if ($errors->count() > 0) {
                return $this->reject((string)$errors);
            }

            switch ($message->getEventType()) {
                case 'automation_rule_created':

                    echo "Processing 'automation_rule_created' event for rule ID: " . $message->getRuleId() . "\n";

                    $this->cacheItemPool->deleteItem('automation_rules_user_'.$message->getUserId());
                    break;
                case 'automation_rule_updated':

                    echo "Processing 'automation_rule_updated' event for rule ID: " . $message->getRuleId() . "\n";

                    $this->cacheItemPool->deleteItem('automation_rules_user_'.$message->getUserId());
                    break;
                case 'automation_rule_deleted':

                    echo "Processing 'automation_rule_deleted' event for rule ID: " . $message->getRuleId() . "\n";

                    $this->cacheItemPool->deleteItem('automation_rules_user_'.$message->getUserId());
                    break;
                default:
                    echo "Unknown event type: " . $message->getEventType() . "\n";
                    break;
            }
        } catch (\Throwable $e) {
            return $this->reject($e->getMessage());
        }

        return self::MSG_ACK;
    }

    private function reject(string $error): int
    {
        echo "Incorrect message: $error";

        return self::MSG_REJECT;
    }
}