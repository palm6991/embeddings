<?php

namespace App\Controller;

use App\Services\AutomationRuleService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\AutomationRule;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Route('/automation-rule')]
class AutomationRuleController extends AbstractController
{
    private AutomationRuleService $automationRuleService;

    public function __construct(
        AutomationRuleService $automationRuleService,
    )
    {
        $this->automationRuleService = $automationRuleService;
    }

    #[Route('/create', name: 'automation_rule_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);

        $automationRule = $this->automationRuleService->createAutomationRule($jsonData);

        return new JsonResponse($automationRule->toArray());
    }

    #[Route('/{id}', name: 'automation_rule_read', methods: ['GET'])]
    #[ParamConverter('automationRule', class: AutomationRule::class)]
    public function read(AutomationRule $automationRule): JsonResponse
    {
        return new JsonResponse($automationRule->toArray());
    }

    #[Route('/', name: 'automation_rule_read_all', methods: ['GET'])]
    public function readAll(): JsonResponse
    {
        $automationRules = $this->automationRuleService->getAllAutomationRules();
        $allRulesArray = array_map(fn($rule) => $rule->toArray(), $automationRules);

        return new JsonResponse($allRulesArray);
    }

    #[Route('/{id}', name: 'automation_rule_update', methods: ['PUT'])]
    #[ParamConverter('automationRule', class: AutomationRule::class)]
    public function update(
        AutomationRule        $automationRule,
        Request               $request,
        AutomationRuleService $automationRuleService
    ): Response
    {
        $data = json_decode($request->getContent(), true);

        $automationRule = $automationRuleService->updateAutomationRule($automationRule, $data);

        return $this->json([
            'message' => 'Automation rule updated successfully.',
            'automation_rule' => $automationRule->toArray(),
        ]);
    }

    #[Route('/{id}', name: 'automation_rule_delete', methods: ['DELETE'])]
    #[ParamConverter('automationRule', class: AutomationRule::class)]
    public function delete(AutomationRule $automationRule): JsonResponse
    {
        $this->automationRuleService->deleteAutomationRule($automationRule);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

}