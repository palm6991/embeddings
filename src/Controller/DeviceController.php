<?php

namespace App\Controller;

use App\Entity\Device;
use App\Services\DeviceService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Route('/device')]
class DeviceController extends AbstractController
{
    private DeviceService $deviceService;

    public function __construct(DeviceService $deviceService)
    {
        $this->deviceService = $deviceService;
    }

    #[Route('/', name: 'device_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $device = $this->deviceService->createDevice($jsonData);

        return new JsonResponse($device->toArray(), Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'device_read', methods: ['GET'])]
    #[ParamConverter('device', class: Device::class)]
    public function read(Device $device): JsonResponse
    {
        return new JsonResponse($device->toArray());
    }

    #[Route('/', name: 'device_read_all', methods: ['GET'])]
    public function readAll(): JsonResponse
    {
        $devices = $this->deviceService->getAllDevices();
        $allDevicesArray = array_map(fn(Device $device) => $device->toArray(), $devices);

        return new JsonResponse($allDevicesArray);
    }

    #[Route('/{id}', name: 'device_update', methods: ['PUT'])]
    #[ParamConverter('device', class: Device::class)]
    public function update(Device $device, Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $updatedDevice = $this->deviceService->updateDevice($device, $jsonData);

        return new JsonResponse($updatedDevice->toArray());
    }

    #[Route('/{id}', name: 'device_delete', methods: ['DELETE'])]
    #[ParamConverter('device', class: Device::class)]
    public function delete(Device $device): JsonResponse
    {
        $this->deviceService->deleteDevice($device);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}