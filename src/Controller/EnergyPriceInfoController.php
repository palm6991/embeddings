<?php

namespace App\Controller;

use App\Entity\EnergyPriceInfo;
use App\Services\EnergyPriceInfoService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Route('/energy-price-info')]
class EnergyPriceInfoController extends AbstractController
{
    private EnergyPriceInfoService $energyPriceInfoService;

    public function __construct(EnergyPriceInfoService $energyPriceInfoService)
    {
        $this->energyPriceInfoService = $energyPriceInfoService;
    }

    #[Route('/', name: 'energy_price_info_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $energyPriceInfo = $this->energyPriceInfoService->createEnergyPriceInfo($jsonData);

        return new JsonResponse($energyPriceInfo->toArray(), Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'energy_price_info_read', methods: ['GET'])]
    #[ParamConverter('energyPriceInfo', class: EnergyPriceInfo::class)]
    public function read(EnergyPriceInfo $energyPriceInfo): JsonResponse
    {
        return new JsonResponse($energyPriceInfo->toArray());
    }

    #[Route('/', name: 'energy_price_info_read_all', methods: ['GET'])]
    public function readAll(): JsonResponse
    {
        $energyPriceInfos = $this->energyPriceInfoService->getAllEnergyPriceInfos();
        $allEnergyPriceInfosArray = array_map(fn(EnergyPriceInfo $info) => $info->toArray(), $energyPriceInfos);

        return new JsonResponse($allEnergyPriceInfosArray);
    }

    #[Route('/{id}', name: 'energy_price_info_update', methods: ['PUT'])]
    #[ParamConverter('energyPriceInfo', class: EnergyPriceInfo::class)]
    public function update(EnergyPriceInfo $energyPriceInfo, Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $updatedEnergyPriceInfo = $this->energyPriceInfoService->updateEnergyPriceInfo($energyPriceInfo, $jsonData);

        return new JsonResponse($updatedEnergyPriceInfo->toArray());
    }

    #[Route('/{id}', name: 'energy_price_info_delete', methods: ['DELETE'])]
    #[ParamConverter('energyPriceInfo', class: EnergyPriceInfo::class)]
    public function delete(EnergyPriceInfo $energyPriceInfo): JsonResponse
    {
        $this->energyPriceInfoService->deleteEnergyPriceInfo($energyPriceInfo);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}