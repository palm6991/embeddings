<?php

namespace App\Controller;

use App\Entity\EnergyUsageLog;
use App\Services\EnergyUsageLogService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Route('/energy-usage-log')]
class EnergyUsageLogController extends AbstractController
{
    private EnergyUsageLogService $energyUsageLogService;

    public function __construct(EnergyUsageLogService $energyUsageLogService)
    {
        $this->energyUsageLogService = $energyUsageLogService;
    }

    #[Route('/', name: 'energy_usage_log_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $energyUsageLog = $this->energyUsageLogService->createEnergyUsageLog($jsonData);

        return new JsonResponse($energyUsageLog->toArray(), Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'energy_usage_log_read', methods: ['GET'])]
    #[ParamConverter('energyUsageLog', class: EnergyUsageLog::class)]
    public function read(EnergyUsageLog $energyUsageLog): JsonResponse
    {
        return new JsonResponse($energyUsageLog->toArray());
    }

    #[Route('/', name: 'energy_usage_log_read_all', methods: ['GET'])]
    public function readAll(): JsonResponse
    {
        $energyUsageLogs = $this->energyUsageLogService->getAllEnergyUsageLogs();
        $allEnergyUsageLogsArray = array_map(fn(EnergyUsageLog $log) => $log->toArray(), $energyUsageLogs);

        return new JsonResponse($allEnergyUsageLogsArray);
    }

    #[Route('/{id}', name: 'energy_usage_log_update', methods: ['PUT'])]
    #[ParamConverter('energyUsageLog', class: EnergyUsageLog::class)]
    public function update(EnergyUsageLog $energyUsageLog, Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $updatedEnergyUsageLog = $this->energyUsageLogService->updateEnergyUsageLog($energyUsageLog, $jsonData);

        return new JsonResponse($updatedEnergyUsageLog->toArray());
    }

    #[Route('/{id}', name: 'energy_usage_log_delete', methods: ['DELETE'])]
    #[ParamConverter('energyUsageLog', class: EnergyUsageLog::class)]
    public function delete(EnergyUsageLog $energyUsageLog): JsonResponse
    {
        $this->energyUsageLogService->deleteEnergyUsageLog($energyUsageLog);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}