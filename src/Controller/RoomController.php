<?php

namespace App\Controller;

use App\Entity\Room;
use App\Services\RoomService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Route('/room')]
class RoomController extends AbstractController
{
    private RoomService $roomService;

    public function __construct(RoomService $roomService)
    {
        $this->roomService = $roomService;
    }

    #[Route('/', name: 'room_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $room = $this->roomService->createRoom($jsonData);

        return new JsonResponse($room->toArray(), Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'room_read', methods: ['GET'])]
    #[ParamConverter('room', class: Room::class)]
    public function read(Room $room): JsonResponse
    {
        return new JsonResponse($room->toArray());
    }

    #[Route('/', name: 'room_read_all', methods: ['GET'])]
    public function readAll(): JsonResponse
    {
        $rooms = $this->roomService->getAllRooms();
        $allRoomsArray = array_map(fn(Room $room) => $room->toArray(), $rooms);

        return new JsonResponse($allRoomsArray);
    }

    #[Route('/{id}', name: 'room_update', methods: ['PUT'])]
    #[ParamConverter('room', class: Room::class)]
    public function update(Room $room, Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $updatedRoom = $this->roomService->updateRoom($room, $jsonData);

        return new JsonResponse($updatedRoom->toArray());
    }

    #[Route('/{id}', name: 'room_delete', methods: ['DELETE'])]
    #[ParamConverter('room', class: Room::class)]
    public function delete(Room $room): JsonResponse
    {
        $this->roomService->deleteRoom($room);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}