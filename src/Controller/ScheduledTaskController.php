<?php

namespace App\Controller;

use App\Entity\ScheduledTask;
use App\Services\ScheduledTaskService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Route('/scheduled-task')]
class ScheduledTaskController extends AbstractController
{
    private ScheduledTaskService $scheduledTaskService;

    public function __construct(ScheduledTaskService $scheduledTaskService)
    {
        $this->scheduledTaskService = $scheduledTaskService;
    }

    #[Route('/', name: 'scheduled_task_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $scheduledTask = $this->scheduledTaskService->createScheduledTask($jsonData);

        return new JsonResponse($scheduledTask->toArray(), Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'scheduled_task_read', methods: ['GET'])]
    #[ParamConverter('scheduledTask', class: ScheduledTask::class)]
    public function read(ScheduledTask $scheduledTask): JsonResponse
    {
        return new JsonResponse($scheduledTask->toArray());
    }

    #[Route('/', name: 'scheduled_task_read_all', methods: ['GET'])]
    public function readAll(): JsonResponse
    {
        $scheduledTasks = $this->scheduledTaskService->getAllScheduledTasks();
        $allScheduledTasksArray = array_map(fn(ScheduledTask $task) => $task->toArray(), $scheduledTasks);

        return new JsonResponse($allScheduledTasksArray);
    }

    #[Route('/{id}', name: 'scheduled_task_update', methods: ['PUT'])]
    #[ParamConverter('scheduledTask', class: ScheduledTask::class)]
    public function update(ScheduledTask $scheduledTask, Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $updatedScheduledTask = $this->scheduledTaskService->updateScheduledTask($scheduledTask, $jsonData);

        return new JsonResponse($updatedScheduledTask->toArray());
    }

    #[Route('/{id}', name: 'scheduled_task_delete', methods: ['DELETE'])]
    #[ParamConverter('scheduledTask', class: ScheduledTask::class)]
    public function delete(ScheduledTask $scheduledTask): JsonResponse
    {
        $this->scheduledTaskService->deleteScheduledTask($scheduledTask);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}