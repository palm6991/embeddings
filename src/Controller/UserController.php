<?php

namespace App\Controller;

use App\Entity\User;
use App\Services\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Route('/user')]
class UserController extends AbstractController
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    #[Route('/', name: 'user_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $user = $this->userService->createUser($jsonData);

        return new JsonResponse($user->toArray(), Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'user_read', methods: ['GET'])]
    #[ParamConverter('user', class: User::class)]
    public function read(User $user): JsonResponse
    {
        return new JsonResponse($user->toArray());
    }

    #[Route('/', name: 'user_read_all', methods: ['GET'])]
    public function readAll(): JsonResponse
    {
        $users = $this->userService->getAllUsers();
        $allUsersArray = array_map(fn(User $user) => $user->toArray(), $users);

        return new JsonResponse($allUsersArray);
    }

    #[Route('/{id}', name: 'user_update', methods: ['PUT'])]
    #[ParamConverter('user', class: User::class)]
    public function update(User $user, Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $updatedUser = $this->userService->updateUser($user, $jsonData);

        return new JsonResponse($updatedUser->toArray());
    }

    #[Route('/{id}', name: 'user_delete', methods: ['DELETE'])]
    #[ParamConverter('user', class: User::class)]
    public function delete(User $user): JsonResponse
    {
        $this->userService->deleteUser($user);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
