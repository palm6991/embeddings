<?php

namespace App\Controller;

use App\Entity\WeatherData;
use App\Services\WeatherDataService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Route('/weather-data')]
class WeatherDataController extends AbstractController
{
    private WeatherDataService $weatherDataService;

    public function __construct(WeatherDataService $weatherDataService)
    {
        $this->weatherDataService = $weatherDataService;
    }

    #[Route('/', name: 'weather_data_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $weatherData = $this->weatherDataService->createWeatherData($jsonData);

        return new JsonResponse($weatherData->toArray(), Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'weather_data_read', methods: ['GET'])]
    #[ParamConverter('weatherData', class: WeatherData::class)]
    public function read(WeatherData $weatherData): JsonResponse
    {
        return new JsonResponse($weatherData->toArray());
    }

    #[Route('/', name: 'weather_data_read_all', methods: ['GET'])]
    public function readAll(): JsonResponse
    {
        $weatherDatas = $this->weatherDataService->getAllWeatherData();
        $allWeatherDatasArray = array_map(fn(WeatherData $data) => $data->toArray(), $weatherDatas);

        return new JsonResponse($allWeatherDatasArray);
    }

    #[Route('/{id}', name: 'weather_data_update', methods: ['PUT'])]
    #[ParamConverter('weatherData', class: WeatherData::class)]
    public function update(WeatherData $weatherData, Request $request): JsonResponse
    {
        $jsonData = json_decode($request->getContent(), true);
        $updatedWeatherData = $this->weatherDataService->updateWeatherData($weatherData, $jsonData);

        return new JsonResponse($updatedWeatherData->toArray());
    }

    #[Route('/{id}', name: 'weather_data_delete', methods: ['DELETE'])]
    #[ParamConverter('weatherData', class: WeatherData::class)]
    public function delete(WeatherData $weatherData): JsonResponse
    {
        $this->weatherDataService->deleteWeatherData($weatherData);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}