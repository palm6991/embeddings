<?php

namespace App\DTO;

use JsonException;

class RuleChangeDTO
{
    private array $payload;

    public function __construct(int $ruleId, string $userId, array $data)
    {
        $this->payload = ['ruleId' => $ruleId, 'userId' => $userId, 'eventType' => 'automation_rule_created', 'data' => $data]; //TODO: Temporary hardcode
    }

    /**
     * @throws JsonException
     */
    public function toAMQPMessage(): string
    {
        return json_encode($this->payload, JSON_THROW_ON_ERROR);
    }
}