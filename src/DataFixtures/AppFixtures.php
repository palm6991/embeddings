<?php

namespace App\DataFixtures;

use App\Entity\AutomationRule;
use App\Entity\Device;
use App\Entity\EnergyPriceInfo;
use App\Entity\EnergyUsageLog;
use App\Entity\Room;
use App\Entity\ScheduledTask;
use App\Entity\User;
use App\Entity\WeatherData;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }
    public function load(ObjectManager $manager)
    {

        $roles = [
            ['ROLE_USER', 'ROLE_DEVICE_VIEW', 'ROLE_ROOM_CREATE'],
            ['ROLE_ADMIN', 'ROLE_AUTOMATION_RULE_CREATE', 'ROLE_ENERGY_PRICE_VIEW'],
            ['ROLE_SUPER_ADMIN', 'ROLE_USER_CREATE', 'ROLE_DEVICE_CREATE', 'ROLE_SCHEDULED_TASK_CREATE'],

        ];

        // Creating and persisting User entities
        $users = [];
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setName('User ' . $i);
            $user->setEmail('user' . $i . '@example.com');
            // Hash the password
            $plaintextPassword = 'test'; // Replace 'test' with the chosen plain password
            $hashedPassword = $this->passwordHasher->hashPassword($user, $plaintextPassword);
            $user->setPassword($hashedPassword);

            // ... set other User fields
            // Assign roles to the user. Here we cycle through the predefined roles array.
            $user->setRoles($roles[$i % count($roles)]);

            $manager->persist($user);
            $users[] = $user;
        }

        // Flush to ensure User entities are persisted before trying to use them
        $manager->flush();

        // Create and persist Device entities, associating each with a User
        $devices = [];
        for ($i = 0; $i < 10; $i++) {
            $device = new Device();
            $device->setName('Device ' . $i);
            $device->setUser($users[$i % count($users)]); // Associate with a User
            $device->setDeviceType('type');
            $device->setStatus('active');
            $manager->persist($device);
            $devices[] = $device;
        }

        // Ensure the Device entities are persisted before trying to use them
        $manager->flush();

        // Create EnergyUsageLog entities and associate them with Device entities
        for ($i = 0; $i < 10; $i++) {
            $log = new EnergyUsageLog();
            $log->setTimestamp(new \DateTimeImmutable("2023-01-01T12:00:00Z"));
            $log->setDevice($devices[$i % count($devices)]);
            $log->setEnergyConsumed(10);
            $manager->persist($log);
        }

        $automationRules = [];
        for ($i = 0; $i < 10; $i++) {
            $rule = new AutomationRule();
            $rule->setRuleName('Rule ' . $i);
            $rule->setUser($users[$i % count($users)]);
            $rule->setRuleConditions(['cond1', 'cond2']);
            $rule->setIsActive(true);
            $manager->persist($rule);
            $automationRules[] = $rule;
        }

        // Ensure the AutomationRule entities are persisted before trying to use them
        $manager->flush();

        // Creating 10 random ScheduledTask records and associating them with AutomationRule entities
        for ($i = 0; $i < 10; $i++) {
            $task = new ScheduledTask();
            $task->setTaskStatus('Task ' . $i);
            $task->setScheduledTime(new \DateTime()); // Example execution time
            $task->setRule($automationRules[$i % count($automationRules)]); // Associate with an AutomationRule
            // ... set other ScheduledTask fields
            $manager->persist($task);
        }

        // Creating 10 random EnergyPriceInfo records
        for ($i = 0; $i < 10; $i++) {
            $priceInfo = new EnergyPriceInfo();
            $priceInfo->setPricePerUnit(rand(1, 100)); // Example price
            $priceInfo->setTimePeriodStart(new \DateTimeImmutable("2023-01-01T12:00:00Z"));
            $priceInfo->setTimePeriodEnd(new \DateTimeImmutable("2025-01-01T12:00:00Z"));
            $manager->persist($priceInfo);
        }

        // Creating 10 random Room records
        for ($i = 0; $i < 10; $i++) {
            $room = new Room();
            $room->setName('Room ' . $i);
            $room->setUser($users[$i % count($users)]);
            $manager->persist($room);
        }

        // Creating 10 random WeatherData records
        for ($i = 0; $i < 10; $i++) {
            $weatherData = new WeatherData();
            $weatherData->setTemperature(rand(-10, 35)); // Example temperature
            $weatherData->setTimestamp(new \DateTimeImmutable("2026-01-01T12:00:00Z"));
            $weatherData->setHumidity(10.5);
            $weatherData->setWeatherCondition('good');
            $manager->persist($weatherData);
        }

        $manager->flush();
    }
}
