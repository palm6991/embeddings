<?php

namespace App\Entity;

use App\Repository\AutomationRuleRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use JetBrains\PhpStorm\ArrayShape;

#[ORM\Entity(repositoryClass: AutomationRuleRepository::class)]
#[ORM\Table(name: 'automation_rules')]
#[ORM\Index(columns: ['user_id'], name: 'automation_rules__user_id__ind')]
class AutomationRule
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'automationRules')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    private User $user;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private string $ruleName;

    #[ORM\Column(type: 'json')]
    #[Assert\Type(type: 'array')]
    private array $ruleConditions;

    #[ORM\Column(type: 'boolean')]
    #[Assert\Type(type: 'bool')]
    private bool $isActive;

    #[ORM\OneToMany(mappedBy: 'rule', targetEntity: ScheduledTask::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $scheduledTasks;

    public function __construct()
    {
        $this->scheduledTasks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRuleName(): string
    {
        return $this->ruleName;
    }

    public function setRuleName(string $ruleName): self
    {
        $this->ruleName = $ruleName;

        return $this;
    }

    public function getRuleConditions(): array
    {
        return $this->ruleConditions;
    }

    public function setRuleConditions(array $ruleConditions): self
    {
        $this->ruleConditions = $ruleConditions;

        return $this;
    }

    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getScheduledTasks(): Collection
    {
        return $this->scheduledTasks;
    }

    public function setScheduledTasks(Collection $scheduledTasks): void
    {
        $this->scheduledTasks = $scheduledTasks;
    }

    #[ArrayShape([
        'id' => "int|null",
        'user_id' => "int",
        'ruleName' => "string",
        'ruleConditions' => "array",
        'isActive' => "bool",
        'scheduledTasks' => "array"
    ])]
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'user_id' => $this->getUser()->getId(),
            'ruleName' => $this->getRuleName(),
            'ruleConditions' => $this->getRuleConditions(),
            'isActive' => $this->getIsActive(),
            'scheduledTasks' => array_map(function ($task) {
                return $task->toArray();
            }, $this->getScheduledTasks()->toArray())
        ];
    }
}
