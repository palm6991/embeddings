<?php

namespace App\Entity;

use App\Repository\DeviceRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use JetBrains\PhpStorm\ArrayShape;
use DateTimeInterface;

#[ORM\Entity(repositoryClass: DeviceRepository::class)]
#[ORM\Table(name: 'devices')]
#[ORM\Index(columns: ['user_id'], name: 'devices__user_id__ind')]
#[ORM\HasLifecycleCallbacks]
class Device
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'devices')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    private User $user;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: "The name cannot be blank.")]
    #[Assert\Length(
        max: 255,
        maxMessage: "The name cannot be longer than {{ limit }} characters."
    )]
    private string $name;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: "The device type cannot be blank.")]
    #[Assert\Length(
        max: 255,
        maxMessage: "The device type cannot be longer than {{ limit }} characters."
    )]
    private string $deviceType;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: "The status cannot be blank.")]
    #[Assert\Length(
        max: 255,
        maxMessage: "The status cannot be longer than {{ limit }} characters."
    )]
    private string $status;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?DateTimeInterface $lastActive = null;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\OneToMany(mappedBy: 'device', targetEntity: EnergyUsageLog::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $energyUsageLogs;

    public function __construct()
    {
        // ... Other initializations ...
        $this->energyUsageLogs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDeviceType(): string
    {
        return $this->deviceType;
    }

    public function setDeviceType(string $deviceType): self
    {
        $this->deviceType = $deviceType;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getLastActive(): ?DateTimeInterface
    {
        return $this->lastActive;
    }

    public function setLastActive(?DateTimeInterface $lastActive): self
    {
        $this->lastActive = $lastActive;

        return $this;
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        if ($this->createdAt === null) {
            $this->createdAt = new \DateTimeImmutable();
        }
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getEnergyUsageLogs(): Collection
    {
        return $this->energyUsageLogs;
    }

    public function setEnergyUsageLogs(Collection $energyUsageLogs): void
    {
        $this->energyUsageLogs = $energyUsageLogs;
    }

    #[ArrayShape([
        'id' => "int|null",
        'user_id' => "int",
        'name' => "string",
        'deviceType' => "string",
        'status' => "string",
        'lastActive' => "DateTimeInterface|null",
        'createdAt' => "DateTimeInterface|null",
        'updatedAt' => "DateTimeInterface|null",
        'energyUsageLogs' => "array"
    ])]
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'user_id' => $this->getUser()->getId(),
            'name' => $this->getName(),
            'deviceType' => $this->getDeviceType(),
            'status' => $this->getStatus(),
            'lastActive' => $this->getLastActive()?->format('Y-m-d H:i:s'),
            'createdAt' => $this->createdAt?->format('Y-m-d H:i:s'),
            'updatedAt' => $this->updatedAt?->format('Y-m-d H:i:s'),
            'energyUsageLogs' => array_map(function ($log) {
                return $log->toArray();
            }, $this->getEnergyUsageLogs()->toArray())
        ];
    }
}
