<?php

namespace App\Entity;

use App\Repository\EnergyPriceInfoRepository;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use DateTimeInterface;

#[ORM\Entity(repositoryClass: EnergyPriceInfoRepository::class)]
#[ORM\Table(name: 'energy_price_info')]
#[UniqueEntity(fields: ["timePeriodStart", "timePeriodEnd"], message: "The time period for this price must be unique.")]
class EnergyPriceInfo
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'datetime')]
    #[Assert\DateTime(message: "The start time must be a valid datetime.")]
    private DateTimeInterface $timePeriodStart;

    #[ORM\Column(type: 'datetime')]
    #[Assert\DateTime(message: "The end time must be a valid datetime.")]
    #[Assert\GreaterThan(propertyPath: "timePeriodStart", message: "The end time must be greater than the start time.")]
    private DateTimeInterface $timePeriodEnd;

    #[ORM\Column(type: 'float')]
    #[Assert\Positive(message: "The price per unit must be a positive number.")]
    private float $pricePerUnit;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimePeriodStart(): DateTimeInterface
    {
        return $this->timePeriodStart;
    }

    public function setTimePeriodStart(DateTimeInterface $timePeriodStart): self
    {
        $this->timePeriodStart = $timePeriodStart;

        return $this;
    }

    public function getTimePeriodEnd(): DateTimeInterface
    {
        return $this->timePeriodEnd;
    }

    public function setTimePeriodEnd(DateTimeInterface $timePeriodEnd): self
    {
        $this->timePeriodEnd = $timePeriodEnd;

        return $this;
    }

    public function getPricePerUnit(): float
    {
        return $this->pricePerUnit;
    }

    public function setPricePerUnit(float $pricePerUnit): self
    {
        $this->pricePerUnit = $pricePerUnit;

        return $this;
    }

    #[ArrayShape([
        'id' => "int|null",
        'timePeriodStart' => "string",
        'timePeriodEnd' => "string",
        'pricePerUnit' => "float"
    ])]
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'timePeriodStart' => $this->getTimePeriodStart()->format('Y-m-d H:i:s'),
            'timePeriodEnd' => $this->getTimePeriodEnd()->format('Y-m-d H:i:s'),
            'pricePerUnit' => $this->getPricePerUnit(),
        ];
    }
}