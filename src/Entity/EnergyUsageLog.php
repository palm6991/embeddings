<?php

namespace App\Entity;

use App\Repository\EnergyUsageLogRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JetBrains\PhpStorm\ArrayShape;
use DateTimeInterface;

#[ORM\Entity(repositoryClass: EnergyUsageLogRepository::class)]
#[ORM\Table(name: 'energy_usage_logs')]
#[ORM\Index(columns: ['device_id'], name: 'energy_usage_logs__device_id__ind')]
class EnergyUsageLog
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Device::class)]
    #[ORM\JoinColumn(name: 'device_id', referencedColumnName: 'id', nullable: false)]
    #[Assert\NotNull]
    private Device $device;

    #[ORM\Column(type: 'datetime')]
    #[Assert\DateTime]
    private DateTimeInterface $timestamp;

    #[ORM\Column(type: 'float')]
    #[Assert\PositiveOrZero]
    private float $energyConsumed;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDevice(): Device
    {
        return $this->device;
    }

    public function setDevice(Device $device): self
    {
        $this->device = $device;

        return $this;
    }

    public function getTimestamp(): DateTimeInterface
    {
        return $this->timestamp;
    }

    public function setTimestamp(DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function getEnergyConsumed(): float
    {
        return $this->energyConsumed;
    }

    public function setEnergyConsumed(float $energyConsumed): self
    {
        $this->energyConsumed = $energyConsumed;

        return $this;
    }

    #[ArrayShape([
        'id' => "int|null",
        'device_id' => "int",
        'timestamp' => "string",
        'energyConsumed' => "float"
    ])]
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'device_id' => $this->getDevice()->getId(),
            'timestamp' => $this->getTimestamp()->format('Y-m-d H:i:s'),
            'energyConsumed' => $this->getEnergyConsumed()
        ];
    }
}
