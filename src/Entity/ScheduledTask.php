<?php

namespace App\Entity;

use App\Repository\ScheduledTaskRepository;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\Validator\Constraints as Assert;
use DateTimeInterface;

#[ORM\Entity(repositoryClass: ScheduledTaskRepository::class)]
#[ORM\Table(name: 'scheduled_tasks')]
#[ORM\Index(columns: ['rule_id'], name: 'scheduled_tasks__rule_id__ind')]
class ScheduledTask
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: AutomationRule::class)]
    #[ORM\JoinColumn(name: 'rule_id', referencedColumnName: 'id', nullable: false)]
    #[Assert\NotNull(message: "The task must be associated with an automation rule.")]
    private AutomationRule $rule;

    #[ORM\Column(type: 'datetime')]
    #[Assert\DateTime(message: "The scheduled time must be a valid datetime.")]
    private DateTimeInterface $scheduledTime;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: "The task status cannot be blank.")]
    #[Assert\Length(
        max: 255,
        maxMessage: "The task status cannot be longer than {{ limit }} characters."
    )]
    private string $taskStatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRule(): AutomationRule
    {
        return $this->rule;
    }

    public function setRule(AutomationRule $rule): self
    {
        $this->rule = $rule;

        return $this;
    }

    public function getScheduledTime(): DateTimeInterface
    {
        return $this->scheduledTime;
    }

    public function setScheduledTime(DateTimeInterface $scheduledTime): self
    {
        $this->scheduledTime = $scheduledTime;

        return $this;
    }

    public function getTaskStatus(): string
    {
        return $this->taskStatus;
    }

    public function setTaskStatus(string $taskStatus): self
    {
        $this->taskStatus = $taskStatus;

        return $this;
    }

    #[ArrayShape([
        'id' => "int|null",
        'rule_id' => "int",
        'scheduledTime' => "string",
        'taskStatus' => "string"
    ])]
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'rule_id' => $this->getRule()->getId(),
            'scheduledTime' => $this->getScheduledTime()->format('Y-m-d H:i:s'),
            'taskStatus' => $this->getTaskStatus()
        ];
    }
}
