<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use JetBrains\PhpStorm\ArrayShape;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: 'users')]
#[ORM\HasLifecycleCallbacks]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    #[Assert\NotBlank(message: "Email cannot be blank.")]
    #[Assert\Email(message: "The email '{{ value }}' is not a valid email.")]
    #[Assert\Length(
        max: 255,
        maxMessage: "Email cannot be longer than {{ limit }} characters."
    )]
    private string $email;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: "Password cannot be blank.")]
    private string $password;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: "Name cannot be blank.")]
    #[Assert\Length(
        max: 255,
        maxMessage: "Name cannot be longer than {{ limit }} characters."
    )]
    private string $name;

    #[ORM\Column(type: 'json', length: 1024, nullable: false)]
    private array $roles = [];

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Device::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $devices;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: AutomationRule::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $automationRules;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Room::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $rooms;

    public function __construct()
    {
        $this->devices = new ArrayCollection();
        $this->automationRules = new ArrayCollection();
        $this->rooms = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';
        return array_unique($roles);
    }
    /**
     * @param string[] $roles
     */
    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        if ($this->createdAt === null) {
            $this->createdAt = new \DateTimeImmutable();
        }
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function setDevices(Collection $devices): void
    {
        $this->devices = $devices;
    }

    public function getAutomationRules(): Collection
    {
        return $this->automationRules;
    }

    public function setAutomationRules(Collection $automationRules): void
    {
        $this->automationRules = $automationRules;
    }

    public function getRooms(): Collection
    {
        return $this->rooms;
    }

    public function addRoom(Room $room): self
    {
        if (!$this->rooms->contains($room)) {
            $this->rooms[] = $room;
            $room->setUser($this);
        }

        return $this;
    }

    public function removeRoom(Room $room): self
    {
        if ($this->rooms->removeElement($room)) {
            // Set the owning side to null (unless already changed)
            if ($room->getUser() === $this) {
                $room->setUser(null);
            }
        }

        return $this;
    }

    #[ArrayShape([
        'id' => "int|null",
        'email' => "string",
        'name' => "string",
        'roles' => 'string[]',
        'createdAt' => "string|null",
        'updatedAt' => "string|null",
        'device_ids' => "array",
        'automationRule_ids' => "array",
        'room_ids' => "array"
    ])]
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'email' => $this->getEmail(),
            'name' => $this->getName(),
            'roles' => $this->getRoles(),
            'createdAt' => $this->createdAt?->format('Y-m-d H:i:s'),
            'updatedAt' => $this->updatedAt?->format('Y-m-d H:i:s'),
            'device_ids' => array_map(fn(Device $device) => $device->getId(), $this->getDevices()->toArray()),
            'automationRule_ids' => array_map(fn(AutomationRule $rule) => $rule->getId(), $this->getAutomationRules()->toArray()),
            'room_ids' => array_map(fn(Room $room) => $room->getId(), $this->getRooms()->toArray())
        ];
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }
}