<?php

namespace App\Entity;

use App\Repository\WeatherDataRepository;
use Doctrine\ORM\Mapping as ORM;
use DateTimeInterface;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: WeatherDataRepository::class)]
#[ORM\Table(name: 'weather_data')]
class WeatherData
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'datetime')]
    #[Assert\NotNull(message: "Timestamp is required.")]
    private DateTimeInterface $timestamp;

    #[ORM\Column(type: 'float')]
    #[Assert\NotNull(message: "Temperature is required.")]
    #[Assert\Type(
        type: 'float',
        message: 'The value {{ value }} is not a valid {{ type }}.'
    )]
    private float $temperature;

    #[ORM\Column(type: 'float')]
    #[Assert\NotNull(message: "Humidity is required.")]
    #[Assert\Type(
        type: 'float',
        message: 'The value {{ value }} is not a valid {{ type }}.'
    )]
    private float $humidity;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: "Weather condition is required.")]
    #[Assert\Length(
        max: 255,
        maxMessage: "Weather condition cannot be longer than {{ limit }} characters"
    )]
    private string $weatherCondition;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimestamp(): DateTimeInterface
    {
        return $this->timestamp;
    }

    public function setTimestamp(DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function getTemperature(): float
    {
        return $this->temperature;
    }

    public function setTemperature(float $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getHumidity(): float
    {
        return $this->humidity;
    }

    public function setHumidity(float $humidity): self
    {
        $this->humidity = $humidity;

        return $this;
    }

    public function getWeatherCondition(): string
    {
        return $this->weatherCondition;
    }

    public function setWeatherCondition(string $weatherCondition): self
    {
        $this->weatherCondition = $weatherCondition;

        return $this;
    }

    #[ArrayShape([
        'id' => "int|null",
        'timestamp' => "string",
        'temperature' => "float",
        'humidity' => "float",
        'weatherCondition' => "string"
    ])]
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'timestamp' => $this->getTimestamp()->format('Y-m-d H:i:s'),
            'temperature' => $this->getTemperature(),
            'humidity' => $this->getHumidity(),
            'weatherCondition' => $this->getWeatherCondition()
        ];
    }
}
