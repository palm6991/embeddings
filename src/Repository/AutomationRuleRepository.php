<?php

namespace App\Repository;

use App\Entity\AutomationRule;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AutomationRule|null find($id, $lockMode = null, $lockVersion = null)
 * @method AutomationRule|null findOneBy(array $criteria, array $orderBy = null)
 * @method AutomationRule[]    findAll()
 * @method AutomationRule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AutomationRuleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AutomationRule::class);
    }

    /**
     * Find automation rules by the user.
     *
     * @param int $userId
     * @return AutomationRule[]
     */
    public function findByUser(int $userId): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.user = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find active automation rules for a given user.
     *
     * @param int $userId
     * @return AutomationRule[]
     */
    public function findActiveByUser(int $userId): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.user = :userId')
            ->andWhere('a.isActive = :active')
            ->setParameters([
                'userId' => $userId,
                'active' => true,
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * Find automation rules by name.
     *
     * @param string $ruleName
     * @return AutomationRule[]
     */
    public function findByName(string $ruleName): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.ruleName LIKE :ruleName')
            ->setParameter('ruleName', '%'.$ruleName.'%')
            ->getQuery()
            ->getResult();
    }
}