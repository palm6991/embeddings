<?php

namespace App\Repository;

use App\Entity\EnergyPriceInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTimeInterface;

/**
 * @method EnergyPriceInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method EnergyPriceInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method EnergyPriceInfo[]    findAll()
 * @method EnergyPriceInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnergyPriceInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EnergyPriceInfo::class);
    }

    /**
     * Find energy prices within a specific time period.
     *
     * @param DateTimeInterface $start
     * @param DateTimeInterface $end
     * @return EnergyPriceInfo[]
     */
    public function findByTimePeriod(DateTimeInterface $start, DateTimeInterface $end): array
    {
        return $this->createQueryBuilder('e')
            ->where('e.timePeriodStart >= :start')
            ->andWhere('e.timePeriodEnd <= :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find the latest energy price information.
     *
     * @return EnergyPriceInfo|null
     */
    public function findLatest(): ?EnergyPriceInfo
    {
        return $this->createQueryBuilder('e')
            ->orderBy('e.timePeriodStart', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}