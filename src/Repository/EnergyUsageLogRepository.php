<?php

namespace App\Repository;

use App\Entity\EnergyUsageLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTimeInterface;

/**
 * @method EnergyUsageLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method EnergyUsageLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method EnergyUsageLog[]    findAll()
 * @method EnergyUsageLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnergyUsageLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EnergyUsageLog::class);
    }

    /**
     * Find energy usage logs by device.
     *
     * @param int $deviceId
     * @return EnergyUsageLog[]
     */
    public function findByDevice(int $deviceId): array
    {
        // Uses the 'device_id_idx' index for efficient querying.
        return $this->createQueryBuilder('e')
            ->where('e.device = :deviceId')
            ->setParameter('deviceId', $deviceId)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find energy usage logs within a specific time range for a given device.
     *
     * @param int $deviceId
     * @param DateTimeInterface $startDate
     * @param DateTimeInterface $endDate
     * @return EnergyUsageLog[]
     */
    public function findBetweenDatesForDevice(int $deviceId, DateTimeInterface $startDate, DateTimeInterface $endDate): array
    {
        return $this->createQueryBuilder('e')
            ->where('e.device = :deviceId')
            ->andWhere('e.timestamp BETWEEN :startDate AND :endDate')
            ->setParameters([
                'deviceId' => $deviceId,
                'startDate' => $startDate,
                'endDate' => $endDate,
            ])
            ->getQuery()
            ->getResult();
    }
}