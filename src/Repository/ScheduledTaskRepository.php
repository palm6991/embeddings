<?php

namespace App\Repository;

use App\Entity\ScheduledTask;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTimeInterface;

/**
 * @method ScheduledTask|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScheduledTask|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScheduledTask[]    findAll()
 * @method ScheduledTask[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScheduledTaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ScheduledTask::class);
    }

    /**
     * Find scheduled tasks by rule.
     *
     * @param int $ruleId
     * @return ScheduledTask[]
     */
    public function findByRule(int $ruleId): array
    {
        // This method will use the 'rule_id_idx' index for the query.
        return $this->createQueryBuilder('s')
            ->where('s.rule = :ruleId')
            ->setParameter('ruleId', $ruleId)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find scheduled tasks by their status.
     *
     * @param string $status
     * @return ScheduledTask[]
     */
    public function findByStatus(string $status): array
    {
        return $this->createQueryBuilder('s')
            ->where('s.taskStatus = :status')
            ->setParameter('status', $status)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find scheduled tasks within a specific time range.
     *
     * @param DateTimeInterface $start
     * @param DateTimeInterface $end
     * @return ScheduledTask[]
     */
    public function findBetweenDates(DateTimeInterface $start, DateTimeInterface $end): array
    {
        return $this->createQueryBuilder('s')
            ->where('s.scheduledTime BETWEEN :start AND :end')
            ->setParameters([
                'start' => $start,
                'end' => $end,
            ])
            ->getQuery()
            ->getResult();
    }
}