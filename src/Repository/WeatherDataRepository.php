<?php

namespace App\Repository;

use App\Entity\WeatherData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTimeInterface;

/**
 * @method WeatherData|null find($id, $lockMode = null, $lockVersion = null)
 * @method WeatherData|null findOneBy(array $criteria, array $orderBy = null)
 * @method WeatherData[]    findAll()
 * @method WeatherData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeatherDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WeatherData::class);
    }

    /**
     * Find weather data within a specific time range.
     *
     * @param DateTimeInterface $start
     * @param DateTimeInterface $end
     * @return WeatherData[]
     */
    public function findByTimeRange(DateTimeInterface $start, DateTimeInterface $end): array
    {
        return $this->createQueryBuilder('w')
            ->where('w.timestamp BETWEEN :start AND :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find weather data by temperature range.
     *
     * @param float $minTemp
     * @param float $maxTemp
     * @return WeatherData[]
     */
    public function findByTemperatureRange(float $minTemp, float $maxTemp): array
    {
        return $this->createQueryBuilder('w')
            ->where('w.temperature >= :minTemp AND w.temperature <= :maxTemp')
            ->setParameter('minTemp', $minTemp)
            ->setParameter('maxTemp', $maxTemp)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find weather data by specific weather condition.
     *
     * @param string $condition
     * @return WeatherData[]
     */
    public function findByWeatherCondition(string $condition): array
    {
        return $this->createQueryBuilder('w')
            ->where('w.weatherCondition = :condition')
            ->setParameter('condition', $condition)
            ->getQuery()
            ->getResult();
    }
}