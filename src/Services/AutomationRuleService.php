<?php

namespace App\Services;

use App\DTO\RuleChangeDTO;
use App\Entity\AutomationRule;
use App\Repository\AutomationRuleRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\UserRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AutomationRuleService
{
    private $automationRuleRepository;
    private $entityManager;
    private $userRepository;
    private $asyncService;

    public function __construct(
        AutomationRuleRepository $automationRuleRepository,
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        AsyncService $asyncService

    )
    {
        $this->automationRuleRepository = $automationRuleRepository;
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->asyncService = $asyncService;
    }

    /**
     * @throws \JsonException
     */
    public function createAutomationRule(array $data): AutomationRule
    {
        if (!isset($data['user']) || !$user = $this->userRepository->find($data['user'])) {
            throw new NotFoundHttpException('User not found.');
        }

        $automationRule = new AutomationRule();
        $automationRule->setUser($user);
        $automationRule->setRuleName($data['ruleName']);
        $automationRule->setRuleConditions($data['ruleConditions']);
        $automationRule->setIsActive($data['isActive']);

        $this->entityManager->persist($automationRule);
        $this->entityManager->flush();

        $message = (new RuleChangeDTO($automationRule->getId(), $user->getId(), $data))->toAMQPMessage();
        $this->asyncService->publishToExchange(AsyncService::RULE_CHANGE, $message);

        return $automationRule;
    }

    public function updateAutomationRule(AutomationRule $automationRule, array $data): AutomationRule
    {
        if (array_key_exists('ruleName', $data)) {
            $automationRule->setRuleName($data['ruleName']);
        }
        if (array_key_exists('ruleConditions', $data)) {
            $automationRule->setRuleConditions($data['ruleConditions']);
        }
        if (array_key_exists('isActive', $data)) {
            $automationRule->setIsActive($data['isActive']);
        }

        $this->entityManager->flush();
        return $automationRule;
    }

    public function getAutomationRule(int $id): ?AutomationRule
    {
        return $this->automationRuleRepository->find($id);
    }

    public function getAllAutomationRules(): array
    {
        return $this->automationRuleRepository->findAll();
    }

    public function deleteAutomationRule(AutomationRule $automationRule): void
    {
        $this->entityManager->remove($automationRule);
        $this->entityManager->flush();
    }
}