<?php

namespace App\Services;

use App\Entity\Device;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\CacheItemPoolInterface;

class DeviceService
{
    private EntityManagerInterface $entityManager;
    private UserRepository $userRepository;
    private CacheItemPoolInterface $cache;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository, CacheItemPoolInterface $cache)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->cache = $cache;
    }

    public function createDevice(array $data): Device
    {
        $device = new Device();
        $device->setName($data['name'])
            ->setDeviceType($data['deviceType'])
            ->setStatus($data['status']);

        $user = $this->userRepository->find(1);

        $device->setUser($user);

        $this->entityManager->persist($device);
        $this->entityManager->flush();

        $this->invalidateCache();

        return $device;
    }

    public function getAllDevices(): array
    {
        $cacheItem = $this->cache->getItem('all_devices');

        if (!$cacheItem->isHit()) {

            $devices = $this->entityManager->getRepository(Device::class)->findAll();
            $cacheItem->set($devices);
            $cacheItem->expiresAfter(new \DateInterval('PT1H'));
            $this->cache->save($cacheItem);
        } else {
            $devices = $cacheItem->get();
        }

        return $devices;
    }

    public function updateDevice(Device $device, array $data): Device
    {
        if (isset($data['name'])) {
            $device->setName($data['name']);
        }
        if (isset($data['deviceType'])) {
            $device->setDeviceType($data['deviceType']);
        }
        if (isset($data['status'])) {
            $device->setStatus($data['status']);
        }

        $this->entityManager->flush();

        $this->invalidateCache();

        return $device;
    }

    public function deleteDevice(Device $device): void
    {
        $this->entityManager->remove($device);
        $this->entityManager->flush();

        $this->invalidateCache();
    }

    private function invalidateCache(): void
    {
        $this->cache->deleteItem('all_devices');
    }
}