<?php

namespace App\Services;

use App\Entity\EnergyPriceInfo;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Cache\CacheItemPoolInterface;

class EnergyPriceInfoService
{
    private EntityManagerInterface $entityManager;
    private CacheItemPoolInterface $cache;

    public function __construct(EntityManagerInterface $entityManager, CacheItemPoolInterface $cache)
    {
        $this->entityManager = $entityManager;
        $this->cache = $cache;
    }

    /**
     * @throws Exception
     */
    public function createEnergyPriceInfo(array $data): EnergyPriceInfo
    {
        $energyPriceInfo = new EnergyPriceInfo();
        $energyPriceInfo->setTimePeriodStart(new \DateTimeImmutable($data['timePeriodStart']))
            ->setTimePeriodEnd(new \DateTimeImmutable($data['timePeriodEnd']))
            ->setPricePerUnit($data['pricePerUnit']);

        $this->entityManager->persist($energyPriceInfo);
        $this->entityManager->flush();

        $this->invalidateCache();

        return $energyPriceInfo;
    }

    public function getAllEnergyPriceInfos(): array
    {
        $cacheItem = $this->cache->getItem('energy_price_infos');

        if (!$cacheItem->isHit()) {

            $energyPriceInfos = $this->entityManager->getRepository(EnergyPriceInfo::class)->findAll();
            $cacheItem->set($energyPriceInfos);
            $cacheItem->expiresAfter(new \DateInterval('PT1H'));
            $this->cache->save($cacheItem);
        } else {

            $energyPriceInfos = $cacheItem->get();
        }

        return $energyPriceInfos;
    }

    /**
     * @throws Exception
     */
    public function updateEnergyPriceInfo(EnergyPriceInfo $energyPriceInfo, array $data): EnergyPriceInfo
    {
        if (array_key_exists('timePeriodStart', $data)) {
            $energyPriceInfo->setTimePeriodStart(new \DateTimeImmutable($data['timePeriodStart']));
        }
        if (array_key_exists('timePeriodEnd', $data)) {
            $energyPriceInfo->setTimePeriodEnd(new \DateTimeImmutable($data['timePeriodEnd']));
        }
        if (array_key_exists('pricePerUnit', $data)) {
            $energyPriceInfo->setPricePerUnit($data['pricePerUnit']);
        }

        $this->entityManager->flush();

        $this->invalidateCache();

        return $energyPriceInfo;
    }

    public function deleteEnergyPriceInfo(EnergyPriceInfo $energyPriceInfo): void
    {
        $this->entityManager->remove($energyPriceInfo);
        $this->entityManager->flush();

        $this->invalidateCache();
    }

    private function invalidateCache(): void
    {
        $this->cache->deleteItem('energy_price_infos');
    }
}