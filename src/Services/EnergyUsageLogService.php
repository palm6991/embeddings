<?php

namespace App\Services;

use App\Entity\Device;
use App\Entity\EnergyUsageLog;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EnergyUsageLogService
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @throws Exception
     */
    public function createEnergyUsageLog(array $data): EnergyUsageLog
    {
        $energyUsageLog = new EnergyUsageLog();
        $device = $this->entityManager->getRepository(Device::class)->find($data['deviceId']);

        if (!$device) {
            throw new NotFoundHttpException('Device not found');
        }

        $energyUsageLog->setDevice($device)
            ->setTimestamp(new \DateTimeImmutable($data['timestamp']))
            ->setEnergyConsumed($data['energyConsumed']);

        $this->entityManager->persist($energyUsageLog);
        $this->entityManager->flush();

        return $energyUsageLog;
    }

    public function getAllEnergyUsageLogs(): array
    {
        return $this->entityManager->getRepository(EnergyUsageLog::class)->findAll();
    }

    /**
     * @throws Exception
     */
    public function updateEnergyUsageLog(EnergyUsageLog $energyUsageLog, array $data): EnergyUsageLog
    {
        if (array_key_exists('timestamp', $data)) {
            $energyUsageLog->setTimestamp(new \DateTimeImmutable($data['timestamp']));
        }
        if (array_key_exists('energyConsumed', $data)) {
            $energyUsageLog->setEnergyConsumed($data['energyConsumed']);
        }

        $this->entityManager->flush();

        return $energyUsageLog;
    }

    public function deleteEnergyUsageLog(EnergyUsageLog $energyUsageLog): void
    {
        $this->entityManager->remove($energyUsageLog);
        $this->entityManager->flush();
    }
}