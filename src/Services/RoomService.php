<?php

namespace App\Services;

use App\Entity\Room;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RoomService
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createRoom(array $data): Room
    {
        $room = new Room();
        $user = $this->entityManager->getRepository(User::class)->find($data['userId']);

        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }

        $room->setUser($user)
            ->setName($data['name']);

        $this->entityManager->persist($room);
        $this->entityManager->flush();

        return $room;
    }

    public function getAllRooms(): array
    {
        return $this->entityManager->getRepository(Room::class)->findAll();
    }

    public function updateRoom(Room $room, array $data): Room
    {
        if (array_key_exists('name', $data)) {
            $room->setName($data['name']);
        }

        $this->entityManager->flush();

        return $room;
    }

    public function deleteRoom(Room $room): void
    {
        $this->entityManager->remove($room);
        $this->entityManager->flush();
    }
}