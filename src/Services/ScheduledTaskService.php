<?php

namespace App\Services;

use App\Entity\ScheduledTask;
use App\Entity\AutomationRule;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ScheduledTaskService
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @throws Exception
     */
    public function createScheduledTask(array $data): ScheduledTask
    {
        $scheduledTask = new ScheduledTask();
        $rule = $this->entityManager->getRepository(AutomationRule::class)->find($data['ruleId']);

        if (!$rule) {
            throw new NotFoundHttpException('Automation rule not found');
        }

        $scheduledTask->setRule($rule)
            ->setScheduledTime(new \DateTimeImmutable($data['scheduledTime']))
            ->setTaskStatus($data['taskStatus']);

        $this->entityManager->persist($scheduledTask);
        $this->entityManager->flush();

        return $scheduledTask;
    }

    public function getAllScheduledTasks(): array
    {
        return $this->entityManager->getRepository(ScheduledTask::class)->findAll();
    }

    /**
     * @throws Exception
     */
    public function updateScheduledTask(ScheduledTask $scheduledTask, array $data): ScheduledTask
    {
        if (array_key_exists('scheduledTime', $data)) {
            $scheduledTask->setScheduledTime(new \DateTimeImmutable($data['scheduledTime']));
        }
        if (array_key_exists('taskStatus', $data)) {
            $scheduledTask->setTaskStatus($data['taskStatus']);
        }

        $this->entityManager->flush();

        return $scheduledTask;
    }

    public function deleteScheduledTask(ScheduledTask $scheduledTask): void
    {
        $this->entityManager->remove($scheduledTask);
        $this->entityManager->flush();
    }
}