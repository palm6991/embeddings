<?php

namespace App\Services;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class UserService
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createUser(array $data): User
    {
        $user = new User();
        $user->setEmail($data['email'])
            ->setPassword($data['password'])  // You should hash the password
            ->setName($data['name']);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    public function getAllUsers(): array
    {
        return $this->entityManager->getRepository(User::class)->findAll();
    }

    public function updateUser(User $user, array $data): User
    {
        if (array_key_exists('email', $data)) {
            $user->setEmail($data['email']);
        }
        if (array_key_exists('password', $data)) {
            $user->setPassword($data['password']);  // Remember to hash the password
        }
        if (array_key_exists('name', $data)) {
            $user->setName($data['name']);
        }

        $this->entityManager->flush();

        return $user;
    }

    public function deleteUser(User $user): void
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }
}