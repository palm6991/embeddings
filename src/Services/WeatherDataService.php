<?php

namespace App\Services;

use App\Entity\WeatherData;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\CacheItemPoolInterface;

class WeatherDataService
{
    private EntityManagerInterface $entityManager;
    private CacheItemPoolInterface $cacheItemPool;

    public function __construct(EntityManagerInterface $entityManager, CacheItemPoolInterface $cacheItemPool)
    {
        $this->entityManager = $entityManager;
        $this->cacheItemPool = $cacheItemPool;
    }

    public function createWeatherData(array $data): WeatherData
    {
        $weatherData = new WeatherData();
        $weatherData->setTimestamp(new \DateTimeImmutable($data['timestamp']))
            ->setTemperature($data['temperature'])
            ->setHumidity($data['humidity'])
            ->setWeatherCondition($data['weatherCondition']);

        $this->entityManager->persist($weatherData);
        $this->entityManager->flush();


        $cacheItem = $this->cacheItemPool->getItem('weather_data.all');
        if ($cacheItem->isHit()) {
            $this->invalidateCache();
        }

        return $weatherData;
    }

    public function getAllWeatherData(): array
    {
        $cacheKey = 'weather_data.all';
        $cacheItem = $this->cacheItemPool->getItem($cacheKey);

        if (!$cacheItem->isHit()) {

            $weatherData = $this->entityManager->getRepository(WeatherData::class)->findAll();
            $cacheItem->set($weatherData);
            $cacheItem->expiresAfter(3600);
            $this->cacheItemPool->save($cacheItem);
        } else {

            $weatherData = $cacheItem->get();
        }

        return $weatherData;
    }

    public function updateWeatherData(WeatherData $weatherData, array $data): WeatherData
    {
        if (array_key_exists('timestamp', $data)) {
            $weatherData->setTimestamp(new \DateTimeImmutable($data['timestamp']));
        }
        if (array_key_exists('temperature', $data)) {
            $weatherData->setTemperature($data['temperature']);
        }
        if (array_key_exists('humidity', $data)) {
            $weatherData->setHumidity($data['humidity']);
        }
        if (array_key_exists('weatherCondition', $data)) {
            $weatherData->setWeatherCondition($data['weatherCondition']);
        }

        $this->invalidateCache();

        $this->entityManager->flush();

        return $weatherData;
    }

    public function deleteWeatherData(WeatherData $weatherData): void
    {
        $this->entityManager->remove($weatherData);
        $this->entityManager->flush();

        $this->invalidateCache();
    }

    private function invalidateCache(): void
    {
        $this->cacheItemPool->deleteItem('weather_data.all');
    }
}