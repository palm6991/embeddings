<?php

namespace AcceptanceTests;

use App\Tests\Support\AcceptanceTester;
use Symfony\Component\HttpFoundation\Response;

class TokenCest
{
    public function tryToGetTokenWithValidUser(AcceptanceTester $I): void
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Host', 'ec2-51-20-254-66.eu-north-1.compute.amazonaws.com');
        $credentials = base64_encode("User 4:test");
        $I->haveHttpHeader('Authorization', "Basic $credentials");
        $I->sendPost('/api/v1/token', []);
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'token' => 'string',
        ]);
    }
}