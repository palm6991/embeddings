<?php

namespace App\Tests\Fixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateUserFixture extends Fixture
{
    public const ADMIN = 'User 4';

    public function __construct(private readonly UserPasswordHasherInterface $passwordHasher)
    {

    }

    public function load(ObjectManager $manager)
    {
        $this->addReference(self::ADMIN, $this->makeUser($manager, self::ADMIN));

        $manager->flush();
    }

    private function makeUser(ObjectManager $manager, string $login): User
    {
        $user = new User();
        $user->setName($login);
        $user->setEmail('user@nomail.com');
        $user->setRoles([]);

        $password = $this->passwordHasher->hashPassword($user, 'test');
        $user->setPassword($password);

        $manager->persist($user);

        return $user;
    }
}