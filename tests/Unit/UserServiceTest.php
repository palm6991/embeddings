<?php

namespace App\Tests\Unit;

use App\Entity\User;
use App\Services\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;
class UserServiceTest extends TestCase
{
    private $entityManager;
    private $userRepository;
    private $userService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->userRepository = $this->createMock(ObjectRepository::class);

        $this->entityManager->method('getRepository')->willReturn($this->userRepository);
        $this->userService = new UserService($this->entityManager);
    }

    /**
     * @dataProvider userDataProvider
     */
    public function testCreateUser($userData)
    {
        $this->entityManager->expects($this->once())
            ->method('persist')
            ->with($this->callback(function($user) use ($userData) {
                return $user instanceof User &&
                    $user->getEmail() === $userData['email'] &&
                    $user->getName() === $userData['name'];
            }));

        $this->entityManager->expects($this->once())->method('flush');

        $user = $this->userService->createUser($userData);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($userData['email'], $user->getEmail());
        $this->assertEquals($userData['name'], $user->getName());
    }

    public static function userDataProvider(): array
    {
        return [
            [['email' => 'test@example.com', 'password' => 'hashed_password', 'name' => 'Test User']]
        ];
    }

    /**
     * @dataProvider userListDataProvider
     */
    public function testGetAllUsers($userList)
    {
        $this->userRepository->expects($this->once())
            ->method('findAll')
            ->willReturn($userList);

        $result = $this->userService->getAllUsers();

        foreach ($result as $item) {
            $this->assertInstanceOf(User::class, $item, "The returned array should only contain User objects.");
        }
    }

    public static function userListDataProvider(): array
    {
        return [
            [[new User(), new User()]]
        ];
    }

    /**
     * @dataProvider updateUserDataProvider
     */
    public function testUpdateUser($originalData, $updateData, $expectedResult)
    {
        $user = new User();
        $user->setEmail($originalData['email']);
        $user->setName($originalData['name']);
        $user->setPassword($originalData['password']);

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->once())->method('flush');
        $userService = new UserService($entityManager);

        $updatedUser = $userService->updateUser($user, $updateData);

        $this->assertEquals($expectedResult['email'], $updatedUser->getEmail());
        $this->assertEquals($expectedResult['name'], $updatedUser->getName());
        $this->assertEquals($expectedResult['password'], $updatedUser->getPassword());
    }

    public static function updateUserDataProvider(): array
    {
        return [
            [
                ['email' => 'original@example.com', 'name' => 'Original Name', 'password' => 'originalPassword'],
                ['email' => 'updated@example.com', 'name' => 'Updated Name', 'password' => 'updatedPassword'],
                ['email' => 'updated@example.com', 'name' => 'Updated Name', 'password' => 'updatedPassword']
            ]
        ];
    }

    /**
     * @dataProvider deleteUserDataProvider
     */
    public function testDeleteUser($user)
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->once())
            ->method('remove')
            ->with($this->equalTo($user));
        $entityManager->expects($this->once())
            ->method('flush');

        $userService = new UserService($entityManager);

        $userService->deleteUser($user);
    }

    public static function deleteUserDataProvider(): array
    {
        return [
            [new User()]
        ];
    }
}