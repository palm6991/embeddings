<?php

namespace App\Tests\functional\Controller;

use App\Tests\FixturedTestCase;
use App\Tests\Fixtures\CreateUserFixture;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class TokenControllerTest extends FixturedTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $container = static::getContainer();

        $this->addFixture(new CreateUserFixture($container->get(UserPasswordHasherInterface::class)));
        $this->executeFixtures();
    }

    public static function credentialsProvider(): array
    {
        $username = 'User 4';
        $password = 'test';
        $base64Credentials = base64_encode("$username:$password");

        $headers = [
            'HTTP_AUTHORIZATION' => 'Basic ' . $base64Credentials,
            'CONTENT_TYPE' => 'application/json',
        ];

        return [
            [$username, $password, $headers]
        ];
    }

    public static function invalidCredentialsProvider(): array
    {
        $headers1 = [
            'PHP_AUTH_USER' => 'invalidUser',
            'PHP_AUTH_PW' => 'invalidPassword',
        ];

        $headers2 = [
            'PHP_AUTH_USER' => 'nonExistentUser',
            'PHP_AUTH_PW' => 'wrongPassword',
        ];

        return [
            [$headers1],
            [$headers2]
        ];
    }

    /**
     * @dataProvider credentialsProvider
     */
    public function testGetTokenWithValidCredentials(string $username, string $password, array $headers): void
    {
        static::ensureKernelShutdown();
        $client = static::createClient();

        $client->request('POST', '/api/v1/token', [], [], $headers);

        $this->assertResponseIsSuccessful();
    }

    /**
     * @dataProvider invalidCredentialsProvider
     */
    public function testGetTokenWithInvalidCredentials(array $headers): void
    {
        static::ensureKernelShutdown();
        $client = static::createClient();

        $client->request('POST', '/api/v1/token', [], [], $headers);

        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
        $responseContent = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('message', $responseContent);
        $this->assertSame('Invalid password or username', $responseContent['message']);
    }

    public function testGetTokenWithMissingCredentials(): void
    {
        static::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('POST', '/api/v1/token');

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
        $responseContent = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('message', $responseContent);
        $this->assertSame('Authorization required', $responseContent['message']);
    }
}